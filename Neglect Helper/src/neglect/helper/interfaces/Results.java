/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neglect.helper.interfaces;

import java.awt.Color;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import javax.swing.*;
import net.proteanit.sql.DbUtils;

public class Results extends javax.swing.JPanel {

    private Connection connection1 = null;
    private int count;

    static void visable(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void calcAvg(String selection) {
        int index = 0;
        double time = 0;
        try {
            String query = "select Stimuli from Stimuli where patient = ? AND ID = ?";
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, ProgramState.patientNr);
                pst.setString(2, selection);
                try (ResultSet rs = pst.executeQuery()) {
                    while (rs.next()) {
                        index += rs.getInt("Stimuli");
                    }
                    totStim.setText("" + index);
                    index = 0;
                    pst.close();
                }
            }

            query = "select Correct from Stimuli where patient = ? AND ID = ?";
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, ProgramState.patientNr);
                pst.setString(2, selection);
                try (ResultSet rs = pst.executeQuery()) {
                    while (rs.next()) {
                        index += rs.getInt("Correct");
                    }
                    totCor.setText("" + index);
                    index = 0;
                    pst.close();
                }
            }

            query = "select Reactietijd from Stimuli where patient = ? AND ID = ?";
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, ProgramState.patientNr);
                pst.setString(2, selection);
                try (ResultSet rs = pst.executeQuery()) {
                    while (rs.next()) {
                        if (rs.getDouble("Reactietijd") != 0) {
                            index++;
                        }
                        time += rs.getDouble("Reactietijd");
                    }
                    avgTime.setText(new DecimalFormat("#0.00").format(time / (double) index));
                    time = 0;
                    index = 0;
                    pst.close();
                }
            }

            query = "select Afwijking from Stimuli where patient = ? AND ID = ?";
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, ProgramState.patientNr);
                pst.setString(2, selection);
                try (ResultSet rs = pst.executeQuery()) {
                    while (rs.next()) {
                        if (rs.getDouble("Afwijking") != 0) {
                            index++;
                        }
                        time += rs.getInt("Afwijking");
                    }
                    avgDev.setText(new DecimalFormat("#0.00").format(time / (double) index));
                    pst.close();
                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void countRows() {
        try {
            String query = "SELECT COUNT(*) from Stimuli where patient = ? AND Kolom = 1";
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, ProgramState.patientNr);

                try (ResultSet rs = pst.executeQuery()) {
                    while (rs.next()) {
                        count = rs.getInt(1);
                    }
                    pst.close();
                }
            }
            numberOfSequences.setText(count + "");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fillList() {
        try {

            String query = "select ID from Stimuli where patient = ? AND Kolom = 1";

            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, ProgramState.patientNr);

                try (ResultSet rs = pst.executeQuery()) {
                    DefaultListModel dlm = new DefaultListModel();

                    while (rs.next()) {
                        dlm.addElement(rs.getString("ID"));
                    }
                    SequenceList.setModel(dlm);
                    pst.close();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Results() {
        connection1 = ConnectionDB.Connection();

        initComponents();
        jLabel2.setText(ProgramState.patientNr);
        setBackground(Color.white);
        fillList();
        countRows();

    }

    private void fillTable(String selection) {
        if (selection.equalsIgnoreCase("")) {
            return;
        }

        try {
            String sql = "select Kolom, Stimuli, Correct, Reactietijd, Afwijking from Stimuli where patient = ? AND ID = ?";
            try (PreparedStatement pst = connection1.prepareStatement(sql)) {
                pst.setString(1, ProgramState.patientNr);
                pst.setString(2, selection);
                try (ResultSet rs = pst.executeQuery()) {
                    TableFill.setModel(DbUtils.resultSetToTableModel(rs));

                    pst.close();
                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane5 = new javax.swing.JScrollPane();
        SequenceList = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        numberOfSequences = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        TableFill = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        totStim = new javax.swing.JLabel();
        totCor = new javax.swing.JLabel();
        avgTime = new javax.swing.JLabel();
        avgDev = new javax.swing.JLabel();
        backToMain = new javax.swing.JButton();
        printButton = new javax.swing.JButton();

        SequenceList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        SequenceList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        SequenceList.setToolTipText("");
        SequenceList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                SequenceListValueChanged(evt);
            }
        });
        jScrollPane5.setViewportView(SequenceList);

        jLabel1.setText("Patient:");

        jLabel2.setText("Number");

        jLabel4.setText("Aantal reeksen:");

        numberOfSequences.setText("numberSequence");

        TableFill.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1", null, null, null, null},
                {"2", null, null, null, null},
                {"3", null, null, null, null},
                {"4", null, "", null, null},
                {"5", null, null, null, null},
                {"6", null, null, null, null},
                {"7", null, null, null, null},
                {"8", null, null, null, null},
                {"9", null, null, null, null},
                {"10", null, null, null, null},
                {"11", null, null, null, null},
                {"12", null, null, null, null}
            },
            new String [] {
                "Kolom", "Stimuli", "Correct", "Reactietijd", "Afwijking"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TableFill.setEnabled(false);
        TableFill.setRowSelectionAllowed(false);
        TableFill.getTableHeader().setReorderingAllowed(false);
        jScrollPane6.setViewportView(TableFill);
        if (TableFill.getColumnModel().getColumnCount() > 0) {
            TableFill.getColumnModel().getColumn(0).setResizable(false);
            TableFill.getColumnModel().getColumn(1).setResizable(false);
            TableFill.getColumnModel().getColumn(2).setResizable(false);
            TableFill.getColumnModel().getColumn(3).setResizable(false);
            TableFill.getColumnModel().getColumn(4).setResizable(false);
        }

        jLabel3.setText("Totaal:");

        jLabel6.setText("12");

        totStim.setText("...");

        totCor.setText("...");

        avgTime.setText("...");

        avgDev.setText("...");

        backToMain.setText("Menu");
        backToMain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backToMainActionPerformed(evt);
            }
        });

        printButton.setText("Print resultaat");
        printButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 128, Short.MAX_VALUE)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(numberOfSequences)
                                .addGap(26, 26, 26)
                                .addComponent(backToMain)
                                .addGap(19, 19, 19))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(49, 49, 49)
                                        .addComponent(totStim)
                                        .addGap(53, 53, 53)
                                        .addComponent(totCor)
                                        .addGap(46, 46, 46)
                                        .addComponent(avgTime)
                                        .addGap(39, 39, 39)
                                        .addComponent(avgDev)
                                        .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(printButton)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(numberOfSequences)
                    .addComponent(backToMain))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                    .addComponent(jScrollPane5))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6)
                    .addComponent(totStim)
                    .addComponent(totCor)
                    .addComponent(avgTime)
                    .addComponent(avgDev))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(printButton)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backToMainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backToMainActionPerformed
        ProgramState.switchState("backToMenu");

    }//GEN-LAST:event_backToMainActionPerformed

    private void SequenceListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_SequenceListValueChanged
        boolean adjust = evt.getValueIsAdjusting();
        if (!adjust) {
            JList list = (JList) evt.getSource();
            int selections[] = list.getSelectedIndices();
            Object selectionValues[] = list.getSelectedValues();
            for (int i = 0, n = selections.length; i < n; i++) {
                this.fillTable(selectionValues[i].toString());
                this.calcAvg(selectionValues[i].toString());
            }
        }
    }//GEN-LAST:event_SequenceListValueChanged

    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed

        Object sequenceName = SequenceList.getSelectedValue();
        String sequenceNames = sequenceName.toString();

        MessageFormat header = new MessageFormat("'Patient: " + ProgramState.patientNr + "  |  Reeks: " + sequenceNames);

        try {

            TableFill.print(JTable.PrintMode.NORMAL, header, null);

        } catch (java.awt.print.PrinterException e) {
            System.err.format("cannot print %s%n", e.getMessage());
        }

    }//GEN-LAST:event_printButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList SequenceList;
    private javax.swing.JTable TableFill;
    private javax.swing.JLabel avgDev;
    private javax.swing.JLabel avgTime;
    private javax.swing.JButton backToMain;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JLabel numberOfSequences;
    private javax.swing.JButton printButton;
    private javax.swing.JLabel totCor;
    private javax.swing.JLabel totStim;
    // End of variables declaration//GEN-END:variables
}
