/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neglect.helper.interfaces;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;

/**
 *
 * @author VrijburgS
 */
public class SequenceSettings extends javax.swing.JPanel {

    private ButtonGroup bg1, bg2;
    private String numberSymbol;
    private Connection connection1 = null;
    private boolean numberSplitApplied;
    private int soundOn;

    private void initBg1() {
        bg1 = new ButtonGroup();
        bg1.add(buttonNumber);
        bg1.add(buttonSymbol);
    }

    private void initBg2() {
        bg2 = new ButtonGroup();
        bg2.add(buttonSoundOn);
        bg2.add(buttonSoundOff);
    }

    private boolean CheckForSave() {
        try {
            String query = "select ID from SequenceSettings";
            ResultSet rs;
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                rs = pst.executeQuery();
                while (rs.next()) {
                    if (nameField.getText().equals(rs.getString("ID"))) {
                        JOptionPane.showMessageDialog(null, "Deze naam bestaat al, verander de naam!");
                        pst.close();
                        rs.close();
                        return true;

                    }
                }
                pst.close();
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void LoadSettingsValues() {
        try {
            String query = "select ID, IntervalMin, IntervalMax, ResponseMin, ResponseMax, Repeat, Sound from SequenceSettings where ID = ?";
            ResultSet rs;
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, ProgramState.seqNr);
                rs = pst.executeQuery();
                while (rs.next()) {
                    nameField.setText(rs.getString("ID"));
                    intervalMin.setText(rs.getString("IntervalMin"));
                    intervalMax.setText(rs.getString("IntervalMax"));
                    responseMin.setText(rs.getString("ResponseMin"));
                    responseMax.setText(rs.getString("ResponseMax"));
                    repeatField.setText(rs.getString("Repeat"));
                    soundOn = rs.getInt("Sound");
                }
                pst.close();
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fillTableFromDatabase() {
        try {
            String query = "select YCor, XCor, DisplayORder from Sequences where SequenceID = ?";
            ResultSet rs;
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, ProgramState.seqNr);
                rs = pst.executeQuery();
                while (rs.next()) {

                    if (tableDatabase.getValueAt(rs.getInt("YCor"), rs.getInt("XCor")) != null) {
                        tableDatabase.setValueAt(tableDatabase.getValueAt(rs.getInt("YCor"), rs.getInt("XCor")) + "-" + rs.getInt("DisplayOrder"), rs.getInt("YCor"), rs.getInt("XCor"));
                    } else {
                        tableDatabase.setValueAt(rs.getInt("DisplayOrder"), rs.getInt("YCor"), rs.getInt("XCor"));
                    }
                }
                pst.close();
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveChangedRows() throws SQLException {

        int index = 0;
        String tempIndex = "";
        String remainingIndex = "";
        int k = 0;

        for (int j = 0; j < 4; j++) {
            for (int i = 1; i <= 12; i++) {

                Object obj1 = tableDatabase.getValueAt(j, i);
                if (obj1 != null && !obj1.toString().equalsIgnoreCase("")) {

                    String obj2 = tableDatabase.getValueAt(j, i).toString();
                    if (obj2.contains("-")) {
                        numberSplitApplied = true;
                    }
                    while (obj2.contains("-")) {
                        if (obj2.charAt(k) == '-') {
                            tempIndex = obj2.substring(0, k);
                            remainingIndex = obj2.substring(k + 1, obj2.length());
                            index = Integer.parseInt(tempIndex);
                            sendSequence(i, j, index);
                            obj2 = remainingIndex;
                            k = 0;
                        }
                        k++;
                    }
                    k = 0;
                    if (numberSplitApplied == false) {
                        index = Integer.parseInt(tableDatabase.getValueAt(j, i).toString());
                        sendSequence(i, j, index);
                    } else {
                        index = Integer.parseInt(obj2);
                        numberSplitApplied = false;
                        sendSequence(i, j, index);

                    }
                }
            }
        }
    }

    private void saveSequenceSetting1() {
        try {
            String query = "insert into SequenceSettings (ID, IntervalMin, IntervalMax, ResponseMin, ResponseMax, Repeat, Sound) values (?,?,?,?,?,?,?)";
            PreparedStatement pst = connection1.prepareStatement(query);
            pst.setString(1, nameField.getText());
            pst.setString(2, intervalMin.getText());
            pst.setString(3, intervalMax.getText());
            pst.setString(4, responseMin.getText());
            pst.setString(5, responseMax.getText());
            pst.setString(6, repeatField.getText());
            pst.setInt(7, soundOn);

            pst.execute();

            JOptionPane.showMessageDialog(null, "Reeks toegevoegd");

            pst.close();

        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Er is iets fout gegaan, probeer het opnieuw");

        }

    }

    public SequenceSettings() {
        connection1 = ConnectionDB.Connection();
        initComponents();
        initBg1();
        initBg2();
        fillTableFromDatabase();
        LoadSettingsValues();
        String intervalToolTip = "<html>Het tijdsinterval wat tussen iedere stimuli weergave zit."
                + "<br>De interval word willekeurig gekozen voor iedere stimuli,"
                + "<br>tussen de maximum en miminum waarde.</html>";
        intervalTag.setToolTipText(intervalToolTip);
        if ("Symbol".equals(ProgramState.numberSymbol)) {
            buttonSymbol.setSelected(true);
        }
        if (soundOn == 0) {
            buttonSoundOff.setSelected(true);
        }
        numberSymbol = "Number";
    }

    public void sendSequence(int iPar, int jPar, int indexPar) throws SQLException {
        int i = iPar;
        int j = jPar;
        int index = indexPar;
        PreparedStatement pstm = null;

        int xCor = i;
        int yCor = j;
        pstm = connection1.prepareStatement("insert into Sequences(SequenceID,XCor,YCor,DisplayOrder) values(?,?,?,?)");
        pstm.setString(1, nameField.getText());
        pstm.setInt(2, xCor);
        pstm.setInt(3, yCor);
        pstm.setInt(4, index);
        pstm.executeUpdate();
        pstm.close();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableDatabase = new javax.swing.JTable();
        backToMenu = new javax.swing.JButton();
        nameTag = new javax.swing.JLabel();
        nameField = new javax.swing.JTextField();
        repeatTag = new javax.swing.JLabel();
        repeatField = new javax.swing.JTextField();
        apply = new javax.swing.JButton();
        intervalTag = new javax.swing.JLabel();
        intervalMin = new javax.swing.JTextField();
        intervalMax = new javax.swing.JTextField();
        tilde1 = new javax.swing.JLabel();
        ms1 = new javax.swing.JLabel();
        responseTag = new javax.swing.JLabel();
        responseMin = new javax.swing.JTextField();
        tilde2 = new javax.swing.JLabel();
        responseMax = new javax.swing.JTextField();
        ms2 = new javax.swing.JLabel();
        sequenceName = new javax.swing.JLabel();
        buttonNumber = new javax.swing.JRadioButton();
        buttonSymbol = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        buttonSoundOn = new javax.swing.JRadioButton();
        buttonSoundOff = new javax.swing.JRadioButton();

        setBackground(new java.awt.Color(255, 255, 255));

        tableDatabase.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"A", null, null, null, null, null, null, null, null, null, null, null, null},
                {"B", null, null, null, null, null, null, null, null, null, null, null, null},
                {"C", null, null, null, null, null, null, null, null, null, null, null, null},
                {"D", null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "", "12", "11", "10", "9", "8", "7", "6", "5", "4", "3", "2", "1"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, true, true, true, true, true, true, true, true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableDatabase);
        if (tableDatabase.getColumnModel().getColumnCount() > 0) {
            tableDatabase.getColumnModel().getColumn(0).setResizable(false);
            tableDatabase.getColumnModel().getColumn(1).setResizable(false);
            tableDatabase.getColumnModel().getColumn(2).setResizable(false);
            tableDatabase.getColumnModel().getColumn(3).setResizable(false);
            tableDatabase.getColumnModel().getColumn(4).setResizable(false);
            tableDatabase.getColumnModel().getColumn(5).setResizable(false);
            tableDatabase.getColumnModel().getColumn(6).setResizable(false);
            tableDatabase.getColumnModel().getColumn(7).setResizable(false);
            tableDatabase.getColumnModel().getColumn(8).setResizable(false);
            tableDatabase.getColumnModel().getColumn(9).setResizable(false);
            tableDatabase.getColumnModel().getColumn(10).setResizable(false);
            tableDatabase.getColumnModel().getColumn(11).setResizable(false);
            tableDatabase.getColumnModel().getColumn(12).setResizable(false);
        }

        backToMenu.setText("Menu");
        backToMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backToMenuActionPerformed(evt);
            }
        });

        nameTag.setText("Reeks naam:");

        nameField.setText("naam");

        repeatTag.setText("Herhalen:");

        repeatField.setText("6");

        apply.setText("Pas veranderingen toe");
        apply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyActionPerformed(evt);
            }
        });

        intervalTag.setText("Stimuli interval:");
        intervalTag.setToolTipText("");

        intervalMin.setToolTipText("Minimum");

        intervalMax.setToolTipText("Maximum");

        tilde1.setText("~");

        ms1.setText("ms");
        ms1.setToolTipText("Milliseconden");

        responseTag.setText("Reactietijd:");

        responseMin.setToolTipText("Minimum");

        tilde2.setText("~");

        responseMax.setToolTipText("Maximum");

        ms2.setText("ms");
        ms2.setToolTipText("Milliseconden");

        sequenceName.setText("Reeks naam");

        buttonNumber.setBackground(new java.awt.Color(255, 255, 255));
        buttonNumber.setSelected(true);
        buttonNumber.setText("Cijfer");
        buttonNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNumberActionPerformed(evt);
            }
        });

        buttonSymbol.setBackground(new java.awt.Color(255, 255, 255));
        buttonSymbol.setText("Symbol");
        buttonSymbol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSymbolActionPerformed(evt);
            }
        });

        jLabel1.setText("Weergave:");

        jLabel2.setText("Geluid:");

        buttonSoundOn.setBackground(new java.awt.Color(255, 255, 255));
        buttonSoundOn.setSelected(true);
        buttonSoundOn.setText("Aan");
        buttonSoundOn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSoundOnActionPerformed(evt);
            }
        });

        buttonSoundOff.setBackground(new java.awt.Color(255, 255, 255));
        buttonSoundOff.setText("Uit");
        buttonSoundOff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSoundOffActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nameTag)
                                    .addComponent(repeatTag))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(repeatField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(48, 48, 48)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(responseTag)
                                    .addComponent(intervalTag)
                                    .addComponent(jLabel2))
                                .addGap(18, 18, 18))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(8, 8, 8)
                                        .addComponent(buttonNumber)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(buttonSymbol)
                                        .addGap(179, 179, 179))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(backToMenu)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(apply)
                                        .addGap(61, 61, 61)))))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(intervalMin, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tilde1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(intervalMax, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ms1)
                                .addContainerGap(12, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(responseMin, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tilde2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(responseMax, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ms2))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(buttonSoundOn)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(buttonSoundOff)))
                                .addGap(0, 0, Short.MAX_VALUE))))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(sequenceName)
                .addGap(228, 228, 228))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(sequenceName)
                .addGap(17, 17, 17)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 38, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(intervalMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tilde1)
                    .addComponent(intervalMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ms1)
                    .addComponent(intervalTag)
                    .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nameTag))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(repeatTag)
                    .addComponent(repeatField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(responseTag)
                    .addComponent(responseMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tilde2)
                    .addComponent(responseMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ms2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(buttonSoundOn)
                        .addComponent(buttonSoundOff))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(buttonNumber)
                        .addComponent(buttonSymbol)
                        .addComponent(jLabel1)
                        .addComponent(jLabel2)))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backToMenu)
                    .addComponent(apply)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backToMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backToMenuActionPerformed
        ProgramState.numberSymbol = numberSymbol;
        ProgramState.soundOn = soundOn;
        ProgramState.switchState("backToMenu");

    }//GEN-LAST:event_backToMenuActionPerformed

    private void buttonSymbolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSymbolActionPerformed
        numberSymbol = "Symbol";
    }//GEN-LAST:event_buttonSymbolActionPerformed

    private void buttonNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNumberActionPerformed
        numberSymbol = "Number";
    }//GEN-LAST:event_buttonNumberActionPerformed

    private void applyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_applyActionPerformed

        if (CheckForSave() == false) {
            if (intervalMin.getText().equalsIgnoreCase("") || intervalMax.getText().equalsIgnoreCase("")
                    || responseMin.getText().equalsIgnoreCase("") || responseMax.getText().equalsIgnoreCase("") || repeatField.getText().equalsIgnoreCase("")) {
                JOptionPane.showMessageDialog(null, "Er is iets fout gegaan, vul alle velden in!");

            } else {

                try {
                    saveChangedRows();
                } catch (SQLException ex) {
                    Logger.getLogger(SequenceSettings.class.getName()).log(Level.SEVERE, null, ex);
                }
                saveSequenceSetting1();
                ProgramState.switchState("backToMenu");

            }
        }

    }//GEN-LAST:event_applyActionPerformed

    private void buttonSoundOnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSoundOnActionPerformed
        soundOn = 1;
    }//GEN-LAST:event_buttonSoundOnActionPerformed

    private void buttonSoundOffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSoundOffActionPerformed
        soundOn = 0;
    }//GEN-LAST:event_buttonSoundOffActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton apply;
    private javax.swing.JButton backToMenu;
    private javax.swing.JRadioButton buttonNumber;
    private javax.swing.JRadioButton buttonSoundOff;
    private javax.swing.JRadioButton buttonSoundOn;
    private javax.swing.JRadioButton buttonSymbol;
    private javax.swing.JTextField intervalMax;
    private javax.swing.JTextField intervalMin;
    private javax.swing.JLabel intervalTag;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel ms1;
    private javax.swing.JLabel ms2;
    private javax.swing.JTextField nameField;
    private javax.swing.JLabel nameTag;
    private javax.swing.JTextField repeatField;
    private javax.swing.JLabel repeatTag;
    private javax.swing.JTextField responseMax;
    private javax.swing.JTextField responseMin;
    private javax.swing.JLabel responseTag;
    private javax.swing.JLabel sequenceName;
    private javax.swing.JTable tableDatabase;
    private javax.swing.JLabel tilde1;
    private javax.swing.JLabel tilde2;
    // End of variables declaration//GEN-END:variables
}
