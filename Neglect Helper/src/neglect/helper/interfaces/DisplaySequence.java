/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neglect.helper.interfaces;

import java.awt.AWTException;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

/**
 *
 * @author Arjen
 */
public class DisplaySequence extends Canvas implements KeyListener {

    private AudioStream au;
    private Connection connection1;
    private JFrame appWindow;

    private int[][] table;
    private int[] faultNumber, stimuliNumber;
    private int arrayIndex = -1, intervalMax,
            intervalMin, responseMin, responseMax,
            repeatSeq, correctNumber, soundOn;
    private long endTime, startTime;
    private double[] reactionNumber, deviationNumber, smallestNumber;
    private double scaleX = 1d, scaleY = 1d;

    private Random rnd = new Random();
    private boolean spacePressed = false;
    private Timer timer = new Timer(), timer1 = new Timer();
    private ImageIcon raster;

    private final int SPACE_KEY = 32, ESC_KEY = 27, ARRAY_SIZE = 13,
            POS = 0, X = 1, Y = 2, BLACK = 2, TRUE = 1;
    private final double ORIG_SCRN_WIDTH = 640d, ORIG_SCRN_HEIGHT = 480d;
    private final String SOUND_APPEAR = "beep-08b.wav", SOUND_WRONG = "beep-10.wav";

    private static int windowX, windowY;

    DisplaySequence() {

        super();

        connection1 = ConnectionDB.Connection();
        appWindow = new JFrame("Neglect Helper");
        appWindow.setUndecorated(true);
        appWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        appWindow.setVisible(true);
        appWindow.setLayout(new GridLayout());

        appWindow.add(this);
//        setVisible(true);
        appWindow.setExtendedState(JFrame.MAXIMIZED_BOTH);
        appWindow.setVisible(true);

        if (ProgramState.whiteBlack == BLACK) {
            setBackground(Color.BLACK);
        } else {
            setBackground(Color.WHITE);

        }

        addKeyListener(this);

        Rectangle windowSize = appWindow.getBounds();

        this.scaleX = windowSize.width / this.ORIG_SCRN_WIDTH;
        this.scaleY = windowSize.height / this.ORIG_SCRN_HEIGHT;
        windowX = windowSize.width;
        windowY = windowSize.width;
        fillTable();
        fillSettings();
        initArrays();
        click(100, 100);
        raster = new ImageIcon(getClass().getResource("raster.jpg"));
    }

    public static void click(int x, int y) {
        try {
            Robot bot = new Robot();
            bot.mouseMove(x, y);
            bot.mousePress(InputEvent.BUTTON1_MASK);
            bot.mouseRelease(InputEvent.BUTTON1_MASK);
            bot.mouseMove(windowX, windowY);

//        int[] pixels = new int[16 * 16];
//        Image image = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(16, 16, pixels, 0, 16));
//        Cursor transparentCursor = Toolkit.getDefaultToolkit().createCustomCursor(image, new Point(0, 0), "invisibleCursor");
        } catch (AWTException ex) {
            Logger.getLogger(DisplaySequence.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void paint(Graphics g) {
        g.setFont(new Font("Arial", Font.PLAIN, 40));

        if ("001DEMO".equals(ProgramState.seqNr)) {
            g.drawImage(raster.getImage(), 0, 0, appWindow.getWidth(), appWindow.getHeight(), this);
            return;
        }

        if (ProgramState.whiteBlack == 2) {
            g.setColor(Color.WHITE);
        } else {
            g.setColor(Color.BLACK); //BLACK

        }
        if (arrayIndex >= 0 && arrayIndex <= table.length) {//if futher times space pressed
            //checks voor de cijfers printen
            if (arrayIndex == table.length) {
                arrayIndex = 0;
                repeatSeq--;
                if ("Number".equals(ProgramState.numberSymbol)) {
                    for (int j = 0; j < table.length; j++) {
                        table[j][POS] = randomNr();
                    }
                } else {
                    for (int j = 0; j < table.length; j++) {
                        table[j][POS] = rnd.nextInt(2) + 10;
                    }
                }
            }
            if (repeatSeq == 0) {
                updateResults();
                g.drawString("", 0, 0);
                appWindow.dispose();
                return;
            } else {
                //---------------------------------------
                //begin met printen
                try {
                    Thread.sleep(rnd.nextInt((intervalMax - intervalMin) + 1) + intervalMin);
                } catch (InterruptedException ex) {
                    Logger.getLogger(DisplaySequence.class.getName()).log(Level.SEVERE, null, ex);
                }
                timer();
                startTime = System.nanoTime();

                stimuliNumber[table[arrayIndex][1]]++;

                g.drawString("", 0, 0);//clear display from previous string

                if (table[arrayIndex][POS] == 10) {
                    g.drawString("X", (int) ((table[arrayIndex][X]) * 48 * this.scaleX), (int) ((table[arrayIndex][Y]) * 104 * this.scaleY + 80));
                } else if (table[arrayIndex][0] == 11) {
                    g.drawString("O", (int) ((table[arrayIndex][X]) * 48 * this.scaleX), (int) ((table[arrayIndex][Y]) * 104 * this.scaleY + 80));
                } else {
                    g.drawString("" + table[arrayIndex][POS], (int) ((table[arrayIndex][X]) * 48 * this.scaleX), (int) ((table[arrayIndex][Y]) * 104 * this.scaleY + 80));
                }
                if (soundOn == TRUE) {
                    playSound(SOUND_APPEAR);
                }
                arrayIndex++;
            }
        } else if (arrayIndex == -1) {//set for start

            arrayIndex++;
        }
        responseTimer();

    }

    public void sendTimeToArray() {
        try {
            double time;
            endTime = System.nanoTime() - startTime;
            time = (double) Math.round((endTime * 0.000000001) * 1000) / 1000;
            if (time > smallestNumber[table[arrayIndex - 1][X] - 1]) {
                reactionNumber[table[arrayIndex - 1][X] - 1] += time;
            } else if (time >= (responseMax)) {
                smallestNumber[table[arrayIndex - 1][X] - 1] = time;
            } else {
                if (smallestNumber[table[arrayIndex - 1][X] - 1] >= responseMax) {
                    smallestNumber[table[arrayIndex - 1][X] - 1] = time;

                } else {
                    reactionNumber[table[arrayIndex - 1][X] - 1] += smallestNumber[table[arrayIndex - 1][X] - 1];
                    smallestNumber[table[arrayIndex - 1][X] - 1] = time;

                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
        }
    }

    private void fillTable() {
        int i = 0, count = 0;
        String seqNr = ProgramState.seqNr, query;

        try {
            query = "SELECT COUNT(*) FROM Sequences WHERE SequenceID = ?";
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, seqNr);

                try (ResultSet rs = pst.executeQuery()) {
                    while (rs.next()) {
                        count = rs.getInt(1);
                    }
                    pst.close();
                }
            }
            table = new int[count][3];
            if ("Number".equals(ProgramState.numberSymbol)) {
                for (int j = 0; j < count; j++) {
                    table[j][POS] = randomNr();
                }
            } else {
                for (int j = 0; j < count; j++) {
                    table[j][POS] = rnd.nextInt(2) + 10;
                }
            }

            query = "SELECT XCor FROM Sequences WHERE SequenceID = ? ORDER BY DisplayOrder ASC";
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, seqNr);

                if ("L".equals(ProgramState.leftRight)) {
                    try (ResultSet rs = pst.executeQuery()) {
                        while (rs.next()) {
                            table[i][X] = 13 - rs.getInt("XCor");
                            i++;

                        }
                        i = 0;
                        pst.close();
                    }
                } else {
                    try (ResultSet rs = pst.executeQuery()) {
                        while (rs.next()) {
                            table[i][X] = rs.getInt("XCor");
                            i++;

                        }
                        i = 0;
                        pst.close();
                    }
                }
            }

            query = "SELECT YCor FROM Sequences WHERE SequenceID = ? ORDER BY DisplayOrder ASC";
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, seqNr);

                try (ResultSet rs = pst.executeQuery()) {
                    while (rs.next()) {
                        table[i][Y] = rs.getInt("YCor");

                        i++;
                    }
                    pst.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fillSettings() {
        String seqNr = ProgramState.seqNr, query;
        int soundOn = 2;
        try {
            query = "SELECT IntervalMin, IntervalMax, ResponseMin, ResponseMax, Repeat, Sound FROM SequenceSettings WHERE ID = ?";
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                pst.setString(1, seqNr);

                try (ResultSet rs = pst.executeQuery()) {
                    while (rs.next()) {
                        intervalMin = rs.getInt("IntervalMin");
                        intervalMax = rs.getInt("IntervalMax");
                        responseMin = rs.getInt("ResponseMin");
                        responseMax = rs.getInt("ResponseMax") / 1000;
                        repeatSeq = rs.getInt("Repeat");
                        soundOn = rs.getInt("Sound");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (soundOn == ProgramState.soundOn || ProgramState.soundOn == 2) {
            this.soundOn = soundOn;
        } else {
            this.soundOn = ProgramState.soundOn;
        }

    }

    private void playSound(String sound) {
        try {

            InputStream in = new FileInputStream(sound);

            au = new AudioStream(in);
            AudioPlayer.player.start(au);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == SPACE_KEY && !spacePressed && !"001DEMO".equals(ProgramState.seqNr)) {
            spacePressed = true;
            timer.cancel();
            timer.purge();
            timer = new Timer();
            sendTimeToArray();
            repaint();

        }
        if (e.getKeyCode() == ESC_KEY) {
            timer.cancel();
            timer.purge();
            timer1.cancel();
            timer1.purge();
            appWindow.dispose();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private int randomNr() {
        return (int) (Math.random() * 9);
    }

    private void updateResults() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateOnly = new SimpleDateFormat("dd-MMM-yy");
        String valnow = " " + dateOnly.format(cal.getTime());

        for (int i = 12; i > 0; i--) {

            correctNumber = stimuliNumber[i] - faultNumber[i];
            if (stimuliNumber[i] != 0) {
                if (stimuliNumber[i] == 1) {
                    deviationNumber[i] = 0;
                } else {
                    deviationNumber[i] = ((reactionNumber[i - 1]) / (stimuliNumber[i] - 1)) - smallestNumber[i - 1];
                }

                if (reactionNumber[i - 1] >= stimuliNumber[i] * responseMax) {
                    deviationNumber[i] = 0;
                }

                reactionNumber[i - 1] = (reactionNumber[i - 1] + smallestNumber[i - 1]) / stimuliNumber[i];

            }
            if (reactionNumber[i - 1] > responseMax) {
                if (reactionNumber[i - 1] >= stimuliNumber[1] * responseMax) {
                    deviationNumber[i] = 0;
                }
                reactionNumber[i - 1] = responseMax;
            }

            try {
                String query = "insert into Stimuli (ID, Kolom, Stimuli, Correct, Reactietijd, Afwijking, Patient) values (?,?,?,?,?,?,?)";
                try (PreparedStatement pst = connection1.prepareStatement(query)) {
                    pst.setString(1, ProgramState.seqNr + valnow);
                    pst.setInt(2, 13 - i);
                    pst.setInt(3, stimuliNumber[i]);
                    pst.setInt(4, correctNumber);
                    pst.setDouble(5, (double) Math.round((reactionNumber[i - 1]) * 1000) / 1000);
                    pst.setDouble(6, (double) Math.round((deviationNumber[i]) * 1000) / 1000);
                    pst.setInt(7, Integer.parseInt(ProgramState.patientNr));

                    pst.execute();
                    pst.close();
                }
                ProgramState.switchState("backToMenu");

            } catch (SQLException | NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    public void timer() {
        try {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    playSound(SOUND_WRONG);
                    sendTimeToArray();
                    faultNumber[table[arrayIndex - 1][X]]++;
                    repaint();
                }
            }, responseMax * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void responseTimer() {
        try {
            timer1.schedule(new TimerTask() {
                @Override
                public void run() {
                    spacePressed = false;
                }
            }, responseMin);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initArrays() {

        faultNumber = new int[ARRAY_SIZE];
        stimuliNumber = new int[ARRAY_SIZE];
        reactionNumber = new double[ARRAY_SIZE];
        deviationNumber = new double[ARRAY_SIZE];
        smallestNumber = new double[ARRAY_SIZE];

        for (int i = 0; i < faultNumber.length; i++) {
            faultNumber[i] = 0;
        }
        for (int i = 0; i < stimuliNumber.length; i++) {
            stimuliNumber[i] = 0;
        }
        for (int i = 0; i < reactionNumber.length; i++) {
            reactionNumber[i] = 0.0;
        }
        for (int i = 0; i < deviationNumber.length; i++) {
            deviationNumber[i] = 0.0;
        }
        for (int i = 0; i < smallestNumber.length; i++) {

            smallestNumber[i] = (double) responseMax;
        }
    }

}
