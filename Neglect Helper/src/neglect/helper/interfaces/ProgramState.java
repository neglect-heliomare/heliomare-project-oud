package neglect.helper.interfaces;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import java.awt.Container;

public class ProgramState {

    public static JFrame mainFrame;
    public static String patientNr, seqNr, leftRight, numberSymbol = "Number";
    public static int whiteBlack, soundOn = 2;

    public static void switchState(String state) {
        //Remove all the panels
        Container container = ProgramState.mainFrame.getContentPane();
        if (container == null) {
            return;
        }

        container.removeAll();

        if (state.equalsIgnoreCase("menu")) {
            MainMenu menu = new MainMenu();
            ProgramState.mainFrame.add(menu);
        } else if (state.equalsIgnoreCase("results")) {
            Results results = new Results();
            ProgramState.mainFrame.add(results);
        } else if (state.equalsIgnoreCase("sequenceSetting")) {
            SequenceSettings sequenceSetting = new SequenceSettings();
            ProgramState.mainFrame.add(sequenceSetting);
        } else if (state.equalsIgnoreCase("backToMenu")) {
            MainMenu menu = new MainMenu();
            ProgramState.mainFrame.add(menu);
        } else if (state.equalsIgnoreCase("NewUser")) {
            NewUser newUser = new NewUser();
            ProgramState.mainFrame.add(newUser);
        } else if (state.equalsIgnoreCase("newSequence")) {
            NewSequence newSequence = new NewSequence();
            ProgramState.mainFrame.add(newSequence);
        } else if (state.equalsIgnoreCase("ShowSequence")) {
            DisplaySequence showSequence = new DisplaySequence();
            //ProgramState.mainFrame.add(showSequence);
            MainMenu menu = new MainMenu();
            ProgramState.mainFrame.add(menu);
        }

        //Reinit the frame
        container.invalidate();
        container.repaint();
        ProgramState.mainFrame.pack();
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ProgramState.mainFrame = new MainFrame();
                ProgramState.mainFrame.setLayout(new BorderLayout());
                ProgramState.mainFrame.setDefaultCloseOperation(ProgramState.mainFrame.EXIT_ON_CLOSE);
                ProgramState.mainFrame.setVisible(true);
                ProgramState.switchState("menu");
            }
        });
    }
}
