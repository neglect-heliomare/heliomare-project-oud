package neglect.helper.interfaces;

import java.sql.*;

public class ConnectionDB {

    private static Connection c;
    private static Statement stmt;

    public static Connection Connection() {
        c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return c;
    }

    public static void updateTable() {
        try {
            stmt = c.createStatement();
            String sql = "UPDATE Patient";
            stmt.executeUpdate(sql);
            c.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
