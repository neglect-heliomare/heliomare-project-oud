/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neglect.helper.interfaces;

import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;

/**
 *
 * @author VrijburgS
 */
public class NewUser extends javax.swing.JPanel {

    private ButtonGroup bg1;
    private String LeftRight;

    private void initBg() {

        bg1 = new ButtonGroup();
        setBackground(Color.white);

        bg1.add(leftButton);
        bg1.add(rightButton);

    }
    Connection connection1 = null;

    private boolean checkForSave() {
        try {
            String query = "select PatientID from Patient";
            ResultSet rs;
            try (PreparedStatement pst = connection1.prepareStatement(query)) {
                rs = pst.executeQuery();
                while (rs.next()) {
                    if (patientNumber.getText().equals(rs.getString("PatientID"))) {
                        JOptionPane.showMessageDialog(null, "Dit patientnummer bestaat al!");
                        pst.close();
                        rs.close();
                        return true;

                    }
                }
                pst.close();
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public NewUser() {
        connection1 = ConnectionDB.Connection();

        initComponents();
        initBg();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        backToMenu = new javax.swing.JButton();
        labelPatientID = new javax.swing.JLabel();
        labelNeglect = new javax.swing.JLabel();
        leftButton = new javax.swing.JRadioButton();
        rightButton = new javax.swing.JRadioButton();
        addPatient = new javax.swing.JButton();
        patientNumber = new javax.swing.JFormattedTextField();

        backToMenu.setText("Cancel");
        backToMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backToMenuActionPerformed(evt);
            }
        });

        labelPatientID.setText("Patient ID");

        labelNeglect.setText("Neglect kant");

        leftButton.setBackground(new java.awt.Color(255, 255, 255));
        leftButton.setText("Links");
        leftButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leftButtonActionPerformed(evt);
            }
        });

        rightButton.setBackground(new java.awt.Color(255, 255, 255));
        rightButton.setText("Rechts");
        rightButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rightButtonActionPerformed(evt);
            }
        });

        addPatient.setText("Toevoegen");
        addPatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addPatientActionPerformed(evt);
            }
        });

        patientNumber.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(labelPatientID)
                .addGap(99, 99, 99)
                .addComponent(backToMenu)
                .addGap(23, 23, 23))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(120, 120, 120)
                        .addComponent(leftButton)
                        .addGap(32, 32, 32)
                        .addComponent(rightButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(148, 148, 148)
                        .addComponent(addPatient))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(148, 148, 148)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(patientNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelNeglect))))
                .addContainerGap(140, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backToMenu)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(labelPatientID)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(patientNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44)))
                .addComponent(labelNeglect)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rightButton)
                    .addComponent(leftButton))
                .addGap(18, 18, 18)
                .addComponent(addPatient)
                .addContainerGap(90, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backToMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backToMenuActionPerformed

        ProgramState.switchState("backToMenu");

    }//GEN-LAST:event_backToMenuActionPerformed

    private void leftButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leftButtonActionPerformed
        LeftRight = "R";
    }//GEN-LAST:event_leftButtonActionPerformed

    private void addPatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addPatientActionPerformed

        try {
            if (LeftRight != "") {
                if (checkForSave() == false) {
                    String query = "insert into Patient (PatientID, LeftRight) values (?,?)";
                    PreparedStatement pst = connection1.prepareStatement(query);
                    pst.setString(1, patientNumber.getText());
                    pst.setString(2, LeftRight);
                    pst.execute();

                    JOptionPane.showMessageDialog(null, "Patient toegevoegd");

                    pst.close();
                    ProgramState.switchState("backToMenu");

                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Vul een Patientnummer en neglect kant in!");
            ProgramState.switchState("NewUser");
        }

    }//GEN-LAST:event_addPatientActionPerformed

    private void rightButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rightButtonActionPerformed
        LeftRight = "L";
    }//GEN-LAST:event_rightButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addPatient;
    private javax.swing.JButton backToMenu;
    private javax.swing.JLabel labelNeglect;
    private javax.swing.JLabel labelPatientID;
    private javax.swing.JRadioButton leftButton;
    private javax.swing.JFormattedTextField patientNumber;
    private javax.swing.JRadioButton rightButton;
    // End of variables declaration//GEN-END:variables
}
